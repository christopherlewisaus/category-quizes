$ = jQuery;

if (!String.prototype.format) {
  String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) { 
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
    });
  };
}

function get_max( x ) {
	var max_key = -1;
	var max_value = -1;
	$.each( x, function( key, value ) {
		if( max_key == -1 || value > max_value ) {
			max_key = key;
			max_value = value;
		}
	} );

	return max_key;
}

function quiz_complete() {
	var scores = {}
	var questions = $( ".question" );
	for( var i = 0; i < questions.length; i++ ) {
		if( $( questions[ i ] ).find( ".click" ).length == 0 ) {
			console.log( 'quiz not complete' )
			return false;
		} else {
			var result = $( questions[ i ] ).find( ".click" )

			// process as one category
			var weights = result.attr( "weighted_categories" );
			if( weights != "" ) {
				if( weights.match( /^([0-9]+(\.[0-9]+)?\*[A-Za-z ]+;?)+$/ ) ) {
					$( weights.split( ";" ) ).each( function() {
						var components = this.split( "*" );
						var multiplier = parseFloat( components[ 0 ] );
						var label = components[ 1 ];

						if( typeof scores[ label ] == "undefined" ) {
							scores[ label ] = multiplier;
						} else {
							scores[ label ] += multiplier;
						}
					} );
				} else {
					alert( "Entry invalid: " + result.attr( "weighted_categories" ) );
				}
			}
			// process as a category, but only if it -- you know -- exists
			else if( result.is( "[category]" ) && result.attr( "category" ) != "" ) {
				if( typeof scores[ result.attr( "category" ) ] == "undefined" ) {
					scores[ result.attr( "category" ) ] = 1;
				} else {
					scores[ result.attr( "category" ) ]++;
				}
			}
		}
	}
	console.log( scores );
	return get_max( scores );
}

$( document ).ready( function() {
	// handle image answers
	$( ".question > .answers > .answer img" ).each( function() {
		// we're wrapped in an <a>, go one further
		if( $( this ).parent( "a" ).length > 0 ) {
			var answer = $( this ).parent().parent();
		} else {
			var answer = $( this ).parent();
		}
		answer.html( this );
		answer.addClass( "img" );
	} );

	// handle answer selection

	$( ".question > .answers > .answer" ).click( function( event ) {
		$( this ).parent().find( ".answer" ).removeClass( "click" ).addClass( "not_clicked" );
		$( this ).removeClass( "not_clicked" ).addClass( "click" );

		var result = quiz_complete();
		if( result ) { // if quiz has been finished display appropriate message
			$( ".message" ).hide();
			$( ".message#" + result.replace( / /g, "_" ) ).show();
		}
	} );

	// handle sharing

	$( ".message .share" ).click( function() {
		if( !FB.ui ) {
			alert( "FB.ui needs to be installed." );
		}

		// find message
		var message = $( this ).parent()
		while( message.attr( "class" ) != "message" ) {
			message = message.parent();
		}

		// title
		if( message.attr( "share_title" ) == "" ) {
			var share_title = $( "#title" ).text().trim() + " " + message.attr( "id" ).replace( "_", " " );
		} else {
			var share_title = message.attr( "share_title" );
		}

		// description
		if( message.attr( "share_description" ) == "" ) {
			var share_description = message.find( "p" ).text();
		} else {
			var share_description = message.attr( "share_description" );
		}

		var message = {
			method: 'feed',
			name: share_title,
			link: $( location ).attr( "href" ),
			picture: ( message.find( "img" ).length > 0 ) ? message.find( "img" ).first().attr( "src" ) : "",
			description: share_description,
		};
		console.log( message );
		FB.ui( message );
	} );
} );
