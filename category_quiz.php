<?php
/*
Plugin Name: Category Quizes
Plugin URI: http://www.imge.com
Description: 
Version: 1.0
Author: Chris Lewis
Author URI: http://www.imge.com
*/

function quiz_scripts() {
    wp_enqueue_style( 'quiz-stylesheet', plugin_dir_url( __FILE__ ) . "quiz.css" );
    wp_enqueue_script( 'quiz-script', plugin_dir_url( __FILE__ ) . 'quiz.js', array( 'jquery' ) );

}
add_action( 'wp_enqueue_scripts', 'quiz_scripts' );

function question_shortcode( $atts, $content = null ) {
    $options = shortcode_atts( array(
        'text' => '',
    ), $atts );
    return sprintf( "<div class='question'><h1>%s</h1><ul class='answers'>%s<ul></div>", $options[ "text" ], do_shortcode( $content ) );
}
add_shortcode( 'question', 'question_shortcode' );

function answer_shortcode( $atts, $content = null ) {
    $options = shortcode_atts( array(
        'text' => '',
        'category' => '',
        'weighted_categories' => '',
    ), $atts );

    return sprintf( "<li class='answer' category='%s' weighted_categories='%s'>%s%s</li>", $options[ "category" ], $options[ 'weighted_categories' ], $options[ "text" ], do_shortcode( $content ) );
}
add_shortcode( 'answer', 'answer_shortcode' );

function message_shortcode( $atts, $content = null ) {
    $options = shortcode_atts( array(
        'category' => '',
        'share_title' => '',
        'share_description' => '',
    ), $atts );
    return sprintf( "<div class='message' share_title=\"%s\" share_description=\"%s\" id=\"%s\">%s<button class='shareButt fb share'>Share your Results</button></div>", $options[ "share_title" ], $options[ "share_description" ], str_replace( " ", "_", $options[ "category" ] ), do_shortcode( $content ) );
}
add_shortcode( 'message', 'message_shortcode' );
add_shortcode( 'result', 'message_shortcode' );

?>
